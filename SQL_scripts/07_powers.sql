use SuperheroesDB

INSERT INTO Power(Name,Description) VALUES('Strength','Superhuman strength, can bend steel');

INSERT INTO Power(Name,Description) VALUES('Speed','Superhuman speed, can run faster then a bullet');

INSERT INTO Power(Name,Description) VALUES('Fly','can fly high up in the sky');

INSERT INTO Power(Name,Description) VALUES('Intelligent','more intelligent than the average joe');

INSERT INTO LinkingSuperheroPower(SuperheroId,PowerId) VALUES(1,1);
INSERT INTO LinkingSuperheroPower(SuperheroId,PowerId) VALUES(1,2);
INSERT INTO LinkingSuperheroPower(SuperheroId,PowerId) VALUES(1,3);
INSERT INTO LinkingSuperheroPower(SuperheroId,PowerId) VALUES(3,4);
INSERT INTO LinkingSuperheroPower(SuperheroId,PowerId) VALUES(2,1);
INSERT INTO LinkingSuperheroPower(SuperheroId,PowerId) VALUES(2,2);
INSERT INTO LinkingSuperheroPower(SuperheroId,PowerId) VALUES(2,4);
USE SuperheroesDB;

ALTER TABLE Assistant
ADD SuperheroRefId int;

ALTER TABLE Assistant
ADD CONSTRAINT PK_Superhero_Assistant
FOREIGN KEY (SuperheroRefId) REFERENCES Superhero(SuperheroId);



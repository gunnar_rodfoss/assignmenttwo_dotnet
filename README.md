## Assignment 2 
Made by [Gunnar Rødfoss](https://gitlab.com/gunnar_rodfoss) 😊
In this repo there is two folders.
- The AssignmentTwo folder containes a C# console application that lets you access and perform tasks on a Chinooc database.
- The SQL_scripts contains several SQL-scripts that builds a Superhero database.

This was a assignment given by Noroff to practise SQL and the SQLClient library.

![microsoft sql server](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXMAAACICAMAAAAiRvvOAAABX1BMVEX///8jHyAAAAAWEBIeGhvW1tYhHB4OBQh6eXmenZ709PQZFBU8OjtraWkbFhf8/Pzo6OgtKirw8PDe3d12dHWSkZHj4+PQ0NARCgwMAAVMSkqysbHP09mgn5++vb0qJierqqpWVFQ5NjfHx8e6wMhgXl+CgYG3vcaPnKjX2+CLiYrL0Nacp7KXo67IAADPAADhAAD98/LwXExlY2Sqs72nAADnwMayABm2AAC9ACT56+3EAAnqys/kcHPwpab639+oBDPoRzrnPzKiAADhHR/uZ1vxaFq5jp3hs7ucEjqvnanSjJjBS2C7Ij7NbHnhpKu7tL3Zk5zEP1LahY7IqrLDACTSWGPNLj7JABvMPErjnKHms7jVaHLDGzW6ABHaPETigYfeVlyxAC3rlZbPdYKsABziNTnfJCnjTlLuiIa+WW3lJxvtfHbIeonzsa6zPlbxjYb4wr3vdmvLS1vjvcRJ5d7uAAAWBklEQVR4nO1d+V/TytpPp2lD09IJKU330n2hbC1VsCC1Fb2e16Nc9eKGC4qI54Ac4J7///POM5O1a4qXo2K+P9DJZDKdfvPMs80kcJwDBw4cOHDgwIEDBw4cOHDgwIEDBw4cOHDgwIEDBw4cOHDgwIEDB9cB4vcewK8GXK3VHNL/SSiFgsyJ1e89jF8HYq1ZxVCofe+R/CpQyk1Nvmv4u47kV0F1oSkZB8p3HMmvguTigplm2VEuVw1lsdIsNAu1qqzplJr8XQd0/dGsLMrVGiclC2UgXhIxV3U8l6sEXqwsEvEuUHUiStVkrVZLVpOOi36F+FelQhVJ2dDhWJSSjqBfIR789n/0Ezd10nG1KjuCfpV4eOPR15t1wrQq6ST4lxx38Yrx8MbvhzcePd75z7+T5EiqURGXq46gXyUerm22nzxdf7dx67/PnvwnxCoVR9CvFE/Wb3Jc69Hem92NpaWN3efPdjZbWPruCQCcTlzjOGFnfYfjXrUI8Zuv37zY2Hi5sbHx6s3rvfakvHsipumhRGDOhFHocoPyIIQSl7v0p8De+h6380w/evn8wwsi8kToCfM3W/b7CaA548CLpsnfIkpfbkyuOUUEOb+2hmVvvd1aUsv7u632PofbewfPd5eIxL97+2Zn0143cR7ltXIa+WfJB76kmIdQCj5kV5geipFL9vMD4/US92IPCpsbz8nfZbW6vrnz5u2t3d13a7/vtMf3Eo/PZbVyID43+w0DijDFoiAffOQzeY/vG3r7MfHm+evb5GN/+QCO3t80zhBV8+LVu3dr7z5+HSfu8YBPU98elEAWziWPli3Gilqh9J/TKy2cSxGfZ5XLTPyjfnTs7i9z+MMyI/v2gVpbP1h63rq5z7X23txaW1v7+HWktMdzIppixWIQU87DiNKZcROT6AWNE8kgFCEVPKmIZuCcFCfFOJQSUCmQSok0FhDC7COU5nwZH3f9LGp7afv8y4oqyQcf6Ac+WH5PavZf04Obnw7XDtf+ejC8j/gMN4uo0VNQysz5NCrmQ/ksJpyHo5lUiCuxCgTaeiaaDqWh7bxeKabm0Wwmw6XCaDqTkiNyWAlfQznnbh8dbWtSvPWZ/l1ePoXPl7pOufnnjcPDj0+G+ZCE8xBTwCWERYPzNCqpLdIIgTMSoT4Nx2XJWQ+zlqCOtEqF6iYoS6w7X5pLpK+fFeXay0dHH7YY61vbxJp+Xrldh4PNpbrRqv7kzsfDw53BXRDOuaxACiIh2cR5HGkt0mgePmYRC3cihNKQpvenmBqilT02FCfS15FyjvtytH18vNL947zOnW7jk5Xtc1b/5pWlWevPwzuHd24O6IByngeyfIQ+g3OsCjAHnFN3Pedmh7RNHGU9cBBQnXva3Mr5tcXpytbBl+Pt7e3jk5NO5/hE1SD15dd6k9aDx4c3Hn/97bfDu38OUDDAOZfLcVxslTNxLuuqhXBOHfi5ADuk9OISQlki4q642ggVfxnO2ytEsOvnJ91up9PpnmnV+8tqJLr57NH62iei2+884B7ePbzT7zlSzhPIkwDPxOBcNMs55TwXZYeqmyOXEE/k3GWq/EU4546pxeTwaadBSO+cUkmur1AfZnN/d33tGaX560fyp/XX4d0+D4ZyzqGpLIixSZ/PCFoLlfMppKiHqgMYJvVaJdVOvwrn95hX3rrXadwDUe8eENb/IP5j+9nLpY3Hqlxv3v2bfu4c3n3Y0wHj3Ieo0jZxnmHijLHGuQfReBXnBMxRa5ohMyPEnHQOKjXOJUMtXU+cUH3S7jROO+d4i0h6d/u0vfJ57/3y8qs9TX3XD/9SS+07d79aO4hTKygiF/sw/PMiimfSYaRzTu7LTCqd4kEH5f2ptA/laOUcKXqhUuOccwupzHXO5R8A5+fde/XzDlHhlPXt7aOjleV9U/T59IZ+gP/qkfTpIv2Yz8BfMQBuYSrA4tAZhIIkAMrnIqxpHoLPaWBTypLSlGit5Dw5lo/0kAuvpZeoAjg/B+N5cY9VbHW7JFDaMnson26YaX7cr9OHAff5OXhAifvuSyX/MLbOuNMusaP1BjOm7YtO54gESiYe9tafWi75etdGutHBcJxtnXYhDjoF1UIYJz5j9+Tz9vZnfdmi/e5W3XoNdWIcXBon9zpUau8R9dI663RODjodDt9eOTpSSa+/Xe8T60+f/tlBXjPcb1BuzxttvNVoXLRx4xgoPj/WSP/vuiUOam3+/eDh17u9sZGSmYrn5mZy2dlUnhsEnE/NZnMzc7lsKTPIPmKFQBpwog8R32x8ZmYmUCxlPHba/3CodxpnoDkuzs7vNy4Ikxddptdb77dpnvHZupplwZsPHv555+7dwz8f/t0r+IkcEtw87/XyQbeAWErLgjTxUwR3UGvBh/tW92H5GY1fZMLhKPsqnnYUvOTC63fFeafRaLS58/sXjfug1rc6J9qp28vPwX5u0tW6x4Ttux//3Nms9/eh5BDvMoGf62mQiaKguYHLK6D5HmfFg1wu99S44aaR4DV3JPTf3h8fZ2dnjfuN83v379Oov925Z5zbX9ppLe3d/PRoff3GjacP/x6yGSDNCOXdQoyIMqGEn7GcJ7eE8RSEFlE/OxD4iKWVLc5LrKdgVBCIsJNS9CfkHB+DjBNQBUMcxoZZjvd3X9zafbe+/vThzQHirSJPeXCjeCmVSpWKAnJbOVdviR+hbMmXSs2vulAUruBRxtzMDufzCHqKoeJ8KuWbIrFU8Gfk/LRD/mw17l+ww4uOLsv45v7nl7svN148G6RODIh0sqNZ3fx5pswbXrgEvSVBVDRMayiMokCetlbELhvPeR7RW5XStBImVuIn5JwYzPpFg6p0goOuahxbWx9WlpeXdt/sjd1cVAL6rHuIpFWjnKaiKQSsrgpmSsK8Z8sG5y4ebIXFt8mHx43vh8Npl2t3Lxpn7Q7LulDK21tfVgjjX17biTYxcGrVEmYolPIBeUKmkZCRyRrPORVz9NPnvo7Pt1bOz4jZPGsQroHy9lbj+Hhl+/beSIViAOS4x2aaEQBdjgYJo4cqipzleDTnU27S4qfP8v7x5aJRP+gQ/dg+rtc7rfrp/W73uPvHpmw76xSOjnIeMkBsbDBPVOsYKn085zmiWlBkVIufAO2VlQPu4JhKdKN9f+ui2+02DkCjyLY3JM+6rWrZAhwjCiQYGHGpV9Du7njO6T2yFan+wPj8uc0dbDMlctLoEMLVXRcTcD5NiBOGcZ4ZqYFFiyn4qTnHks2dxLc/wJ4ioLy11eh2OwdtWbtStq1cqJIdpltm+JE8zhO9xGuzYDznbjJp0OBkjg5PuBiYmwus+ix+khwiYFvNfNPFWR+mFf1ZH1pt3FWcLsVnXDPxqYSFUePa/PzqaikNu1nT+czogTHsbXPc+/ekcH7v+Pj45BwTnkX1ftnnPBUbbkNDYxwNxXx+POfZ4Dgbmg8gwQ85Hb+A4qb8VxhyOcQS4Fly3u9GYggqUN9vnIZa/a6GowgyRHzQjVDJ1HYONl3CiHMo6vejLCdFwumSbCPz016ut7f3ufrB0crK5y14iA6ebMFMq4i2OWe8Dv4+n0C0eXzgKQawirA7FDCec+jOhYZrPVw0p32CJm8JrkQeTpyjoZgLiVycH2CGwO/Vc0Ue3pzZicaMWxjgXV66j4p+W7DIJbhwft7Gzsr6Untnea91e2Vl5TbLy4rsaSIMCsY+51R/eKMD1SwIZmzUUEC5uNVtMOM5l+D2+rPDTstzbvA+BZBVmlwwogKV84Df5fW7/TzhPEH6CvZ2BZWxlFbmaUYD4IesnOExMc5B2Hi3O+gvcmluPp0Yzzl+u/P8xevby8tHB9oNFPWt4jIWZdtPl1CXjw8OymTTKTBqLdns3NuIQ6dATKPZwUMT+SAwE094ZDkS9kbN9plyHkohl+AulqZzZK6wUK5nzoCMqHX0V/nRbFqSlfQs8O/VWzPOAzyPZmanitEsJyYyXHj8MyGvXr3dfbWx/OFBldMWCmST8ZDtc86tCjQL0r8NiMpldNSEYQqdlW1wLvIge35+oB2NEynno/qpKTOplPOIm1ejgYjIHNVYytIDjNc/bQwsltUYUXJE1N3atADOc3nkVnczkK9UEvn02MDhv7tvb+0uvWnjKryKi9Fi4lyehHMuzhJWgd4bHUGWQHMARJP7ZyevqNAJ70Wz/aPzwZfxJsGFSaH1B5z7Z93INBnp4KyRA7sztJgjEi9MG6fwnCkeA84DxWh8st0KT2+93X253+K4KvCtPooraQMmyly0629SZBEzWj1UwPz0F0deCXpX1T628udSEHQ2UbOpnhNyv4vk9VJrCaDW12+19GCHrBeQGlXRQVxhDeUg1+NXU3cB0DR+96SU31p7Bn65mIQvxVWQNKyw8VHPZTLOiS/GMyosCiYjjM8UeqlxA9hbJ8Ls/rrQnNVVghREzKre4OtV14Rxbr39UCeYr/AYFpRKtdUtiIOuZzQD58Nj78HDfrp26xNL0arvbJGrQLMCPWKZei/yZJxzkRmBUTFjUmuZ2Nic1NyknIMM+umqFCqaLaDf6/LGrJJH1TNbYGVeZqTvtEXzldyaAQChdvcszaYEPSCjct67/DgSrUc3Hv+bvc5C1l7ZIlVFTlbARVeFXZ74KfSwRoVBm13OJ9AtdGxFbVYZokYv7v0q0rlKKnDuDfacpl6KyQYRd1y1oJT9HpsI3xBlHj9wHp0kb9++8XtLZG8o4pL6G1sI6YokSoomOpNzzkkqFbGc1klG0AVtGGD1VFWqtjkncjijKhjdyvkMKTSQ1dWBb5CWA3NjyojCoWZBiZH097SWDdkHzsflIMz4GzbtSwV4gwin1Iw3E8lV2F2iEy1d5m0L+bkYW2lWhWdg3GGFKbKcgHMy09msimm+A81uKqIFeNqvaWXgvC84w0SwTcK/6nfx7PkDGo5msbU32fgxlHP7b4J4Qvd6KlTOsekluaJUrSomFS5dLn03Txd/+Bi7GtTiaL0nT+afm69ktjSq3lJm1nrg10MyOg368hMWFSIaFlRhy649MFZY4MsE2zL5L9UrLBTI36om5pgQTiiXqgbRl+SciwiwLhTMGWNHo4yxx+QkT8Y5c8hJ90w5BC2bXkwwcd4XtFi+kXo5xowbCNWTBP/crgnFTfX3i+UCsZkF+rpWQngyWZXAdSFFdcZge5vYBkCeoYtxTGBQv7dghdmbnJRztjyqkkqL3gEwcd6fnSAOuVd7oJL4gqoFpeHSwN6CBufDVyMtwAu6yBUKVa5GxJywDG/5k9XgSGf98pxzImS5VQsEPu1IA081rup9TMw5I51dAiXvIJpGcg55aNUYKoYFVTkf0Bs/Ked40fRy3HKhWqgpyQJhnFpLUX//mSwR11xULv+yoLQh3RCojAr+wYrpscfknFPLycwBjTmHNxzCuWwkWMhQ9VWAcSOxz/mC+YW4BYak5p4opvf8YVGWvoFz6nMzaxQao1wSaKJ1oj4oeuJyjCsxhHOu6Ne8SRJ56mEsdBsckbOwzXlh0ShLyTIhvGr4g9jyPksSGSmThaEWmLZEwJrECG+ROgBa5uQSnEP0yfwRWJUdRKqKYZxTnxyuB3Wi3zM8evuIbc6VisqqWC00m2WCgulsNWnICAmOVO1+SUCUrHKeGLGIxM56db/mMpzT1R7ofqADbmAY53DTaBqGyInfSCPmRs8au5wvVuCvVGsuNmsKFpuFgol0XEtqRRmkH3/T+4rpjhfVcoKe9QqDJ40Mzrygm9jLcK7veMmPDr+Gcg4LVXQFw2vWgVOj9o/Y5rxaKYjJZgX4hsNyE2Mi6Zow07eIciDiTMHL1W/Z0bBqckbYcsvgJVHY4uU1MqKX4dxIv49ekhrKOVgclIFhmvfMp/tz65aB2+O8WSHyrRNZW4A8Yhm8dIBYo+ZVSmomVanaN6F9igNbMtlFtylatDSLu616ZyznYl+Kgwa6PC2CCzN8qXQo58AfmR9ETMxpXeZPDRV0m5wvVkzqW1qkjOBCuUxvQ5LcDZE46vo9qSbtq/N4tmdOwMQ0HEQcpEu5ud6JE5qhlJtSgeM579v3TLcNMOXEXKTeWCBlXpsbyDkNP2GWWOLlsDBg2oha7zY5r1QW9DJe0LR3slwmRRKQKrWC2YpO8lbuooBK5tWWUo/ZZMtpPAqbf5M0RWsFs0M2MB1rhogEf8rUi8xmilozG+25h+Q+xNFYziHNIiQEswXlVEH3WnOHCcG8HmqD8wX1jeeActkYNrGkUg0cdbMykZITqPNiEB6x8EVkYnrlSDhIl+bNXIbc9DGLKJpOhESMSaNUFtEVNrRq7gg49+amrVjNGA0IPd4YWs14oBcpPUUf39CX9rGL3sUZbcuVmM6i4HjOIRTms3xvbpaGuF60qtlVxTeH3Nqvssl5oVJpqsXkgvlEjXrqVlWSnOSfchVpUktAKBqMqvtKogFLf1KOrSH5EULuYAyhGHv8qEcT0MyS12+FuQ1dsHb5BegFunJZBVubUCgwWyqt5hAyNk6M4hzY5fvdcZZB86NgtlSayrqQwBt+kU3OxUqlwjSAXDEIkWvEUydavWaRa5PnaANxQcu6qak9Lyr2WoN5RhDNYaiFIMr1cDAwmxe1cB7s6SVouW2Km+3S4v1ut5/uDxA0r2gE5zSu6t11wcESIF2B8QbdbvpcmmkFzK5/TgS9QqW3ovFLfMeFZkEplKtSrVw2vfpcqk3yKENoFSG/nkn1uhE/wN5L00gwpVv5GIr3+Tse5O+HhdR0AAnGI498FMV7HpiZRVF985yXTCv9Yh/pexjnYQQ7GPsdNU/A9Lt4N8rp2icX9btdY4nhqEY3/m8IEM6cR6kJDg0mRrSgOYjJwmRBqJyZ9sJbKWlif3pI0ClniqBbBDr/i4NezhIqTvfDrM+hjS8b074qN99PojI/p53mpxPGz0isTk8Xh3jvCnzvwI3F+VVB/7KSKWs0CyMb3JkVeBFIl0CZi8mFSlMV7KaWU+dkJVmrkSgUFyZRLdrFkXQikY6Mtr0KaQS7GIKrI5uNBpbyiUwi7xlmcqR8OjN2JLa/LDT6y8ZeD5K+wOHkwmLBFB1Z3UJRrhYK/5vxDoZCA/7RG42uE8pEpzfLZidFXihYWohKjbiOVzoI6oXFvkXSfy5UCekLZmvRXDDfACriuHzFDwOy511s6cNrAQzuS1lnvaYHSlgibiNV8Ur5ql/UFKakT5jN+pkhUgWjphHVnLpYLS+UtUj0MhZ0QkwLfWH6NQeuEQ+mAv9FkSpzpbCwYApExeY3rBDZBUuU/IRP6n8DpAI4joVFMVnWMuoakjUu70spxMtOX52OwXNDn46+ziBhP5H2SqE3Uw5inhDFHIdzVyjwEnvY/5q/oXUQpGRhATyZck3RHtmSQN0kZDmTSGSu8n8zedh+tF+QdAAJPwvNRcp8VRHLINwZWU6nMv/E/8P61d5laQWWpWo1yXYwekQxJMuXDngdOHDgwIEDBw4cOHDgwIEDBw4cOHDgwIEDBw4cOHDgwIEDB/8T/D/4fDy/4T2+NwAAAABJRU5ErkJggg==)

![.NET](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACoCAMAAABt9SM9AAAAkFBMVEVnIXr///9bAHFlHHhiFHZfAnNjFndZAG9hAHXe1eHPwNSRa51kGXeVcqHd0uHWyNp1PoVgDHSHWJWigqz39PfZzd3Ht823or+3nr/v6vGvlbhUAGvk2+dxM4KMYZmskLWlhq/CrsiWcaLx7PLq4uyaeaWBUJB5QoltLH9NAGWIW5Z9S42+qcVzN4SaeqW1oL1fYXSSAAAFoUlEQVR4nO2ba3eiOhSGs5NARkRtLF7xUotWT9XO//93ZydBxdt8cM2a6eD7fKgxEFb3s8hmE1AIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFBXlFUPj42MiX7jv/LNkJXorDGSXb0MXh61Fe1GlNTWllyOir4MbTssRiMrZEaZefRoRLSUIo4ePze/MYajIxVCY0tEkZBt6smHDha/E431XqWz/sPn5neGZWU0tq6pV9xkWSLaPjiR7IAoVypuEtVVVodGftaZCTedLCu9PP7kHKZEKqWyLplxizvKGeY3pq6lXcuNSPOflP0nYzMj2pTHqBUsa0X0mbq8TtnYybK79oAjjfVgkhWdrZ6122qeNaSQ3WSZLftT3qjkmDf2uqlQ5rVdZJOxVGm3zSmr0272GkS93rB+tlhWs0MJnzdyQTsvSzaozTn6gzyvkmXOebIauQs9NNMiHvnWmxV2GToLpd9D6y18tB/Le98ZlvXFsyZVIiLaD48JXsUc73w6HawilkXJauik9babJuc1HfGOq83+rZlKnrnD6ZS/d8zLKiF6f11/8be3r1n6t2P77bCs17zgFK9faZKfZLlc/SGVsrF2knJt5YiWeaqiLks0C05OUapTtScaGKUMn3Wc0n5ygtcqYvtTWz9XXpYZcoo3S/oyJ1lcMbVDseVkzWJO3yzJpSFTUOJS+GgeR0LznJtyxk+3RK3YGeZBdb4avlrBZ9Ga47QVWRn1QwHhZHHonP/pXfN3yRdNKX1iGsgwghM+q1zpZ5ClOfH0F7SIqrIKn/XFSdbGa+URI7fF6jln9oWZh5JWsevWc8iKZ5RlHF9VFiduV0bFaSnL1fe9nDumbjparWyeUJZ/uuslb2RpG1WRtVc1tOVlubOFGkZUZLE/Wq6NbDWjUlbEJgZRPuUTik+hrx85++R6gkc2jWE9HSkqst7yaf1sBVl27FJOVZY4VFW+zvKTipOVuzdydVaUhNbcpms61FnqIEto1/Hg/eV3Ro4ytqRUNuLY7DgrrJCdzK2yyJUrN9tb28qykIHksOCeztoK/eWK0mWLx8SbhLUVO7cGxuP9eoV+565+DVdqpHGXOOXv/dyClu/ycWqpRcT3hrEx5YyyMvY9YZuVbiTf+XA5JoOZMN5tFnENXQEAwD9F5DmvibTrKts2Okddd906RC35EaokUQ2VCy1Gh3a5XHWkm151BWpYhF5SyupVH+e4W52jrMaFk4/UF6fXbJ5GFr1XFoEh6w4HWRSfYv2lLExDJjlNxGtZzf1LyZpL9vLL3q03U2dabvlbEfxBjrLcY4iSa1ldrQ5wX9myTS9ZnjbUHS/Ly8mON3M3ZN1aUI9LWX/qX/37eFnJwv1dHMKGrDt4Wb088xe6OPRB1h28rHbu4y7KuCHrDkGWNIn73IXAIesOpSz/AJpo7aVcy9rKwx1gxdrTyhLWv6iw9MXWtaykX5JUHso/ryxhvKGxqx9+VcHPT7dFTyxLCS9jr34t6w2yXMDW371MDGTd5SRLmHZp41pWJznQRM7yAbu3Gcgttdy4GhpZgqth+ZKe9a/G9AzqrDtUZblXcJlVDlm3OZOlpn4i4sy6w5ksEc1DCQpZNzmXVU7EBmRVSOXhnf4LWe5lyBLICthuZ7ILC6MXsoQc35K1zc2B067PISv99LW6j/JSljDLG7KyE8vjvs8hK1ztwty6knV8le/Oo7Di+BToqWR93JQl5ACyKqRdf8k7TcPJWcByVJV1+fj59LvN2C+BdWouS9iPpDEMCT5dtVqt85/ZqBfuaq3Cg8B01jqnGR/327uxnzX82ck5p9JBxFrri3iVdhx21efEF/vV3hUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOA38T+iY1PGhqgjAwAAAABJRU5ErkJggg==)


The assignment itself you can read under.

Create a database and Data access with SQL Client
access it.
Write SOL scripts and Duild a console application in C#. Follow the guidelines given below, feel 
free to expand on the functionality. It must meet the minimum requirements prescribed.
---
NOTE: This assignment is to be completed in groups of 2. In addition to this, you should complete 
this assignment using Pair Programming.
---
1)  Set up the development environment.
Make sure you have installed at least the following tools:

•  Visual Studio 2019 with .NET 5.
•  SQL Server Management Studio

a) Optional: Database diagrams
can use SSMS to draw relationship diagrams of your database, you can include these in your 
repository.

3)  Use  plain C# to create a console application, and SQL scripts to create a database with the 
following minimum requirements (See Appendix A-B for details):
a) SOL scripts to create a database according to the specifications in Appendix A.
b) A repository implementation in a C# console application to access data from the provided Chinook
database (see Appendix B).
c) Summary (ill) tags for each method you write, explaining what the method does, any exceptions it 
can throw, and what data it returns (if applicable). Vou do not need to write summary tags for 
overloaded methods.
4) Submit
a) Create a GitLab repository containing all your code.
b) Vou can include the generated class diagram in this repository if you have made one.
c) The repository must be either public, or I am added as a Maintainer (ONicholasLennox).
d) Submit only the link to your GitLab repository (not the “clone with SSH").
e) Only one person from each group needs to submit but add the names of both group members in the 
submission.

## Appendix A: SQL scripts to create database.

1) Introduction and overview
Vou are to create several scripts which can be run to create a database, setup some tables in the 
database, add relationships to the tables, and then populate the tables with data.
The database and its theme are surrounding superheroes. The database can be called SuperheroesDb. 
REQUIREMENT: Create a script called Oi_dbCreate.sql that contains a statement to create the 
database.
---
NOTE: This script and all following scripts can be written in SSMS through the query window. You 
can test the scripts and save them from there.
---
---
NOTE: If you are unfamiliar with SQL, the W3 Schools set of tutorials are a fantastic resource.
---
2) Tables
There are three main tables you need to create, this is Superhero, Assistant, and Power. Superhero 
has: Autoincremented integer Id, Name, Alias, Origin.
Assistant has: Autoincremented integer Id, Name.
Power has: Autoincremented integer Id, Name, Description.
REQUIREMENT: Create a script called O2_tableCreate.sql that contains statements to create each of 
these taDles and setup their primary keys.
---
NOTE: At this point in time, no relationships are to be configured aside from primary keys. 
Datatypes should be up to you, and what you think is best.
---
3) Table relationships
Here we are going to include some relationships between the tables. The ALTER SOL keyword will be 
used to change the existing tables to add the keys. A description of the relationships can be seen 
below:

•  One Superhero can have multiple assistants, one assistant has one superhero they 
assist.
•  One Superhero can have many powers, and one power can be present on many Superheroes.
Firstly, to setup the Superhero-assistant relationship, we need to add a foreign key. This 
represents the superhero this assistant is linked to (Superherold in Assistant), and ALTER any 
tables needed to cater for this foreign key, and to setup the constraint.
REQUIREMENT: Create a script called O3_re/ationshipSuperheroAssistant.sql that contains statements 
to ALTER any tables needed to add the foreign key and setup the constraint to configure the 
described relationship between Superhero and assistant.
Finally, to setup the Superhero-power relationship, we need to add a linking table. The name of 
this table is up to you but should be based on convention. This table is there purely for linking, 
meaning it needs to contain only two fields, which are both foreign keys and a composite primary 
key.
REQUIREMENT: Create a script called O4_relationshipSuperheroPomer.sql that contains statements to 
create the linking table. This script should contain any ALTER statements needed to set up the 
foreign key constraints between the linking tables and the Superhero and Power tables.

4) Inserting data
Now it is time to populate the database using INSERT statements. Firstly, we should add 
superheroes.
REQUIREMENT: Create a script called O5_insertSuperheroes.sql that inserts three new superheroes 
into the dotaDase.
Then we need to add assistant data.
REQUIREMENT: Create a script called O6_insertAssistants.sql that inserts three assistants, decide 
on which superheroes these can assist.
Finally, we need to add powers. This requires you to add the powers first, then associate them with 
Superheroes.
REQUIREMENT: Create a script called O7_pomers.sql that inserts four powers. Then the script needs 
to give the superheroes powers. Try have one superhero with multiple powers and one power that is 
used by multiple superheroes, to demonstrate the many-to-many.
5) Updating data
Pick one superhero to update their data.
REQUIREMENT: Create a script called O8_updateSUperfiero.sql where you can update a superheroes 
name. Pick any superhero to do this with.
6) Deleting data
Referential integrity will stop us from deleting any records which have got relationships used. 
Meaning, a superhero cannot be deleted if it is used in the assistant table.
REQUIREMENT: Create a script called O9_deleteAssistont.sql where you can delete any assistant. Vou 
can delete that assistant by name (his name must be unique to avoid deleing multiple assistants), 
this is done to ease working with autoincremented numbers — my PC may skip a number or two.
NOTE: The scripts should be able to be run in order with no errors to create a database with the 
requirements stated. That is how I will initially check for functionality, then I will investigate 
each statement for specifics.

## Appendix B: Reading data with SQL Client 
# 1) Introduction and overview
Some hotshot media mogul has heard of your newly acquired skills in C#. They have contracted you 
and a friend to stride on the edge of copyright glory and start re-making iTunes, but under a 
different name. They have spoken to lawyers and are certain a working prototype should not cause 
any problems and ensured that you will be safe. The lawyer they use is the same that Epic has been 
using, so they are familiar with Apple.
The second part of the assignment deals with manipulating SOL Server data in Visual Studio using a 
library called SOL Client. For this part of the assignment, you are given a dataDose to work with. 
It is called Chinook.
Chinook models the iTunes database of customers purchasing songs. Vou are to create a C# console 
application, install the SOL Client library, and create a repository to interact with the database.
NOTE: These requirements are separate from Appendix A.

# 2) Customer requirements
For customers in the database, the following functionality should be catered for:
1.  Read all the customers in the database, this should display their: Id, first name, last name, 
country, postal code, phone number and email.
2. Read a specific customer from the dotaDase (Dy Id), should display everything listed in the 
above point.
3. Read a specific customer by name. HINT: LIKE keyword can help for partial matches.
4.  Return a page of customers from the dotaDase. This should take in limit and o{/set as 
parameters and make use of the SOL limit and offset keywords to get a subset of the customer data. 
The customer model from above should De reused.
5.  Add a new customer to the database. You also need to add only the fields listed above (our 
customer object)
6. Update an existing customer.
7. Return the number of customers in each country, ordered descending (high to low). i.e. USA: 13,
8. Customers who are the highest spenders (total in invoice table is the largest), ordered 
descending.
9.  For a given customer, their most popular genre (in the case of a tie, display both). Most 
popular in this context means the genre that corresponds to the most tracks from invoices 
associated to that customer.
NOTE: You should create a class (model) for each data structure you intend to use. Do not return a 
formatted string. This is a minimum of: Customer, Customer Country, CustomerSpender, CustomerGenre. 
Place these classes in a Models folder.


# 3) Repositories
You are expected to implement the repository pattern in this assignment. The version of the pattern 
to implement is up to you.
---
NOTE: Consult the provided material for examples of the Repository pattern. You can place all 
repository- related classes in a Data Access folder.
---
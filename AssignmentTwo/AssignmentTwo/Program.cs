﻿using AssignmentTwo.Models;
using AssignmentTwo.Repositories;
using Microsoft.Data.SqlClient;
using System;

namespace AssignmentTwo
{
    internal class Program
    {
        static void Main(string[] args)
        {

            ICostumerRepositorie repository = new CostumerRepositorie();
            Start();
            TESTOne();
            TestSelectAll(repository);//OK
            TESTTwo();
            TestGetCustomerByName(repository);//Ok
            TESTThree();
            TestGetCustomerById(repository);//Ok
            TESTFour();
            TestGetPageCustomers(repository);//Ok
            TESTFive();
            TestAddCustomer(repository);//ok
            TESTSix();
            TestUpdateCustomer(repository);//ok
            TESTSeven();
            TestGetCustomersInCountry(repository);//OK
            TESTEight();
            TestGetCustomersSpending(repository);//OK
            TESTNine();
            TestGetCustomersGenre(repository); //MULIG OK
        }
        /// <summary>
        /// test to se if the GetAllCustomers method works
        /// </summary>
        /// <param name="repository"></param>
        static void TestSelectAll(ICostumerRepositorie repository)
        {
            
            foreach (var item in repository.GetAllCustomers())
            {
                Console.WriteLine(item.Id + " "
                                    + item.FirstName + " "
                                    + item.LastName + " "
                                    + item.Country + " "
                                    + item.Emial);
            }
        }
        /// <summary>
        ///  test to se if the GetCostumerByName method works
        /// </summary>
        /// <param name="repository"></param>
        static void TestGetCustomerByName(ICostumerRepositorie repository)
        {
            Console.WriteLine(repository.GetCostumerByName("Leonie").FirstName + " "
                            + repository.GetCostumerByName("Leonie").LastName + " "
                            + repository.GetCostumerByName("Leonie").Country + " "
                            + repository.GetCostumerByName("Leonie").PostalCode + " "
                            + repository.GetCostumerByName("Leonie").PhoneNumber + " "
                            + repository.GetCostumerByName("Leonie").Emial);
        }
        /// <summary>
        /// test to se if the GetCostumerById method works
        /// </summary>
        /// <param name="repository"></param>
        static void TestGetCustomerById(ICostumerRepositorie repository)
        {
            Console.WriteLine(repository.GetCostumerById(2).FirstName + " "
                            + repository.GetCostumerById(2).LastName + " "
                            + repository.GetCostumerById(2).Country + " "
                            + repository.GetCostumerById(2).PostalCode + " "
                            + repository.GetCostumerById(2).PhoneNumber + " "
                            + repository.GetCostumerById(2).Emial);
        }
        /// <summary>
        /// test to se if the GetPageCustomers method works
        /// </summary>
        /// <param name="repository"></param>
        static void TestGetPageCustomers(ICostumerRepositorie repository)
        {
            foreach (var item in repository.GetPageCustomers(2, 2))
            {
                Console.WriteLine(    item.Id + " "
                                    + item.FirstName + " "
                                    + item.LastName + " "
                                    + item.Country + " "
                                    + item.PostalCode + " "
                                    + item.PhoneNumber + " "
                                    + item.Emial);
            }
        }
        /// <summary>
        /// test to se if the AddCustomer method works
        /// </summary>
        /// <param name="repository"></param>
        static void TestAddCustomer(ICostumerRepositorie repository)
        {
            Console.WriteLine("Added new Customer test passed: " 
                                + repository.AddCustomer(new Customers() { 
                                FirstName="Gunnar",
                                LastName="Rødfoss",
                                Country="Norway",
                                PostalCode="1881",
                                PhoneNumber="+47 999 99 999",
                                Emial="gunnar@mail.no"
                                }));
        }
        /// <summary>
        /// test to se if the UpdateCustomer method works
        /// </summary>
        /// <param name="repository"></param>
        static void TestUpdateCustomer(ICostumerRepositorie repository)
        {
            Console.WriteLine("Added new Customer test passed: "
                    + repository.UpdateCustomer(new Customers()
                    {
                        Id=60,
                        FirstName = "Gunnar",
                        LastName = "Rødfoss",
                        Country = "Norway",
                        PostalCode = "2281",
                        PhoneNumber = "+47 888 88 888",
                        Emial = "gunnar@gmail.no"
                    }));
        }
        /// <summary>
        /// test to se if the GetCustomersInCountry method works
        /// </summary>
        /// <param name="repository"></param>
        static void TestGetCustomersInCountry(ICostumerRepositorie repository)
        {
            foreach (var item in repository.GetCustomersInCountry())
            {
                Console.WriteLine(
                                     item.Country + " "
                                    + item.Count);
            }

        }
        /// <summary>
        /// test to se if the GetCustomersSpending method works
        /// </summary>
        /// <param name="repository"></param>
        static void TestGetCustomersSpending(ICostumerRepositorie repository)
        {
            foreach (var item in repository.GetCustomersSpending())
            {
                Console.WriteLine(
                                     item.FirstName + " "
                                    + item.LastName + " "
                                    + item.Total);
            }
        }
        /// <summary>
        /// test to se if the GetCustomersGenre method works
        /// </summary>
        /// <param name="repository"></param>
        static void TestGetCustomersGenre(ICostumerRepositorie repository)
        {
            foreach (var item in repository.GetCustomersGenre(11))
            {
                Console.WriteLine(    item.Id + " "
                                    + item.FirstName + " "
                                    + item.LastName + " "
                                    + item.Genre + " "
                                    + item.PlayedTimes);
            }

        }
        static void Start()
        {
            Console.WriteLine(@"          _____                .__                                     __ ___________                            
         /  _  \   ______ _____|__| ____   ____   _____   ____   _____/  |\__    ___/_  _  ______                
        /  /_\  \ /  ___//  ___/  |/ ___\ /    \ /     \_/ __ \ /    \   __\|    |  \ \/ \/ /  _ \               
       /    |    \\___ \ \___ \|  / /_/  >   |  \  Y Y  \  ___/|   |  \  |  |    |   \     (  <_> )              
       \____|__  /____  >____  >__\___  /|___|  /__|_|  /\___  >___|  /__|  |____|    \/\_/ \____/               
               \/     \/     \/  /_____/      \/      \/     \/     \/                                           
 ____ ___  _________.___ _______    ________    _________________  .____   _________ .__  .__               __   
|    |   \/   _____/|   |\      \  /  _____/   /   _____/\_____  \ |    |  \_   ___ \|  | |__| ____   _____/  |_ 
|    |   /\_____  \ |   |/   |   \/   \  ___   \_____  \  /  / \  \|    |  /    \  \/|  | |  |/ __ \ /    \   __\
|    |  / /        \|   /    |    \    \_\  \  /        \/   \_/.  \    |__\     \___|  |_|  \  ___/|   |  \  |  
|______/ /_______  /|___\____|__  /\______  / /_______  /\_____\ \_/_______ \______  /____/__|\___  >___|  /__|  
                 \/             \/        \/          \/        \__>       \/      \/             \/     \/      ");
        }
        static void TESTOne()
        {
            Console.WriteLine(@"                                 ______________________ ____________________  ____ 
                                \__    ___/\_   _____//   _____/\__    ___/ /_   |
                                  |    |    |    __)_ \_____  \   |    |     |   |
                                  |    |    |        \/        \  |    |     |   |
                                  |____|   /_______  /_______  /  |____|     |___|
                                                   \/        \/                   ");
        }
        static void TESTTwo()
        {
            Console.WriteLine(@"                                ______________________ ____________________ ________  
                                \__    ___/\_   _____//   _____/\__    ___/ \_____  \ 
                                  |    |    |    __)_ \_____  \   |    |     /  ____/ 
                                  |    |    |        \/        \  |    |    /       \ 
                                  |____|   /_______  /_______  /  |____|    \_______ \
                                                   \/        \/                     \/");
        }
        static void TESTThree()
        {
            Console.WriteLine(@"                                ______________________ ____________________ ________  
                                \__    ___/\_   _____//   _____/\__    ___/ \_____  \ 
                                  |    |    |    __)_ \_____  \   |    |      _(__  < 
                                  |    |    |        \/        \  |    |     /       \
                                  |____|   /_______  /_______  /  |____|    /______  /
                                                   \/        \/                    \/ ");
        }
        static void TESTFour()
        {
            Console.WriteLine(@"                                ______________________ ____________________    _____  
                                \__    ___/\_   _____//   _____/\__    ___/   /  |  | 
                                  |    |    |    __)_ \_____  \   |    |     /   |  |_
                                  |    |    |        \/        \  |    |    /    ^   /
                                  |____|   /_______  /_______  /  |____|    \____   | 
                                                   \/        \/                  |__| ");
        }
        static void TESTFive()
        {
            Console.WriteLine(@"                                ______________________ ____________________  .________
                                \__    ___/\_   _____//   _____/\__    ___/  |   ____/
                                  |    |    |    __)_ \_____  \   |    |     |____  \ 
                                  |    |    |        \/        \  |    |     /       \
                                  |____|   /_______  /_______  /  |____|    /______  /
                                                   \/        \/                    \/ ");
        }
        static void TESTSix()
        {
            Console.WriteLine(@"                                ______________________ ____________________   ________
                                \__    ___/\_   _____//   _____/\__    ___/  /  _____/
                                  |    |    |    __)_ \_____  \   |    |    /   __  \ 
                                  |    |    |        \/        \  |    |    \  |__\  \
                                  |____|   /_______  /_______  /  |____|     \_____  /
                                                   \/        \/                    \/ ");
        }
        static void TESTSeven()
        {
            Console.WriteLine(@"                                ______________________ ____________________ _________ 
                                \__    ___/\_   _____//   _____/\__    ___/ \______  \
                                  |    |    |    __)_ \_____  \   |    |        /    /
                                  |    |    |        \/        \  |    |       /    / 
                                  |____|   /_______  /_______  /  |____|      /____/  
                                                   \/        \/                       ");
        }
        static void TESTEight()
        {
            Console.WriteLine(@"                                ______________________ ____________________   ______  
                                \__    ___/\_   _____//   _____/\__    ___/  /  __  \ 
                                  |    |    |    __)_ \_____  \   |    |     >      < 
                                  |    |    |        \/        \  |    |    /   --   \
                                  |____|   /_______  /_______  /  |____|    \______  /
                                                   \/        \/                    \/ ");
        }
        static void TESTNine()
        {
            Console.WriteLine(@"                                ______________________ ____________________  ________ 
                                \__    ___/\_   _____//   _____/\__    ___/ /   __   \
                                  |    |    |    __)_ \_____  \   |    |    \____    /
                                  |    |    |        \/        \  |    |       /    / 
                                  |____|   /_______  /_______  /  |____|      /____/  
                                                   \/        \/                       ");
        }
    }

}


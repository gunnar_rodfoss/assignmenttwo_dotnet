﻿using AssignmentTwo.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTwo.Repositories
{
    public class CostumerRepositorie : ICostumerRepositorie
    {
        
        /// <summary>
        /// returns all the Cutomers in the Customers tabel
        /// </summary>
        /// <returns></returns>
        public List<Customers> GetAllCustomers()
        {
            List<Customers> customer = new List<Customers>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConectionString()))
                {
                    connection.Open();
                    String sql = "SELECT * FROM Customer";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine(reader.GetName(0) + " " + reader.GetName(1));
                            while (reader.Read())
                            {
                                Customers temp = new Customers();
                                temp.Id = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(7);
                                temp.Emial = reader.GetString(11);
                                customer.Add(temp);
                            }

                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return customer;
        }
        /// <summary>
        /// returns a costumer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Customers GetCostumerById(int id) 
        {
            Customers customer = new Customers();
            try { 
            using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConectionString()))
            {
                connection.Open ();
                String sql = $"SELECT * FROM Customer WHERE CustomerId = {id}";
                using (SqlCommand command = new SqlCommand( sql, connection))
                {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            
                            while (reader.Read())
                            {
                                customer.Id = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(7);
                                customer.PostalCode = reader.GetString(8);
                                customer.PhoneNumber = reader.GetString(9);
                                customer.Emial = reader.GetString(11);
                                

                            }
                        } 
                }
            }
            }
            catch(SqlException ex) 
            {
                throw ex;
            }
            return customer;
        }
        /// <summary>
        /// returns a customer by giving its name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Customers GetCostumerByName(string name)
        {
            Customers customer = new Customers();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConectionString()))
                {
                    connection.Open();
                    String sql = $"SELECT * FROM Customer WHERE FirstName LIKE '%{name}%'";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            
                            while (reader.Read())
                            {
                                customer.Id = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(7);
                                customer.PostalCode = reader.GetString(8);
                                customer.PhoneNumber = reader.GetString(9);
                                customer.Emial = reader.GetString(11);


                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return customer;
        }
        /// <summary>
        /// returns all rows in tabel based on the limit and offset value
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public List<Customers> GetPageCustomers(int limit, int offset)
        {
            List<Customers> customer = new List<Customers>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConectionString()))
                {
                    connection.Open();
                    
                    string sql = $"SELECT * FROM Customer WHERE CustomerId >= {offset} AND CustomerId < {offset} + {limit}";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            
                            while (reader.Read())
                            {
                                
                                Customers temp = new Customers();
                                temp.Id = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(7);
                                temp.PostalCode = reader.GetString(8);
                                temp.PhoneNumber = reader.GetString(9);
                                temp.Emial = reader.GetString(11);
                                customer.Add(temp);
                            }

                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return customer;
        }
        
        /// <summary>
        /// adds a new customer to the Customer tabel
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="country"></param>
        /// <param name="postalCode"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        public bool AddCustomer(Customers customer)
        {
            
            bool success = false;
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConectionString()))
                {
                    connection.Open();

                    String sql = "INSERT INTO Customer(FirstName,LastName,Country,PostalCode,Phone,Email) VALUES(@FirstName,@LastName,@Country,@PostalCode,@PhoneNumber,@Email)";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@PhoneNumber", customer.PhoneNumber);
                        command.Parameters.AddWithValue("@Email", customer.Emial);
                        
                        if (command.ExecuteNonQuery() > 0)
                            success = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return success;
        }
        //6 update a costumer
        /// <summary>
        /// updates a costumer in the costumers tabel
        /// </summary>
        /// <param name="id"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="country"></param>
        /// <param name="postalCode"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        public bool UpdateCustomer(Customers customer)
        {
            
            bool success = false;
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConectionString()))
                {
                    connection.Open();

                    string sql = "UPDATE Customer SET FirstName=@FirstName,LastName=@LastName,Country=@Country,PostalCode=@PostalCode,Phone=@PhoneNumber,Email=@Email WHERE CustomerId=@Id";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Id", customer.Id);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@PhoneNumber", customer.PhoneNumber);
                        command.Parameters.AddWithValue("@Email", customer.Emial);
                        if (command.ExecuteNonQuery() > 0)
                            success = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return success;
        }
        
        /// <summary>
        /// returns a a country and the amount of customers in that country
        /// </summary>
        /// <returns></returns>
        public List<CustomerCountry> GetCustomersInCountry()//public List<Customers> GetCustomersInCountry()
        {   
            List<CustomerCountry> customerCountry = new List<CustomerCountry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConectionString()))
                {
                    connection.Open();
                    String sql = "SELECT Country, count(Country) AS CountOf FROM Customer GROUP BY Country";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine(reader.GetName(0) + " " + reader.GetName(1));
                            while (reader.Read())
                            {
                                
                                CustomerCountry temp = new CustomerCountry();
                                temp.Country = reader.GetString(0);
                                temp.Count = reader.GetInt32(1);
                                customerCountry.Add(temp);
                                
                            }

                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            
            return customerCountry;
        }
        /// <summary>
        /// returns the highest spending customers in decending order
        /// </summary>
        /// <returns></returns>
        public List<CustomerSpender> GetCustomersSpending()//public List<Customers> GetCustomersInCountry()
        {
            
            List<CustomerSpender> customerSpender = new List<CustomerSpender>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConectionString()))
                {
                    connection.Open();
                    string sql = "SELECT C.FirstName,C.LastName, I.Total FROM Invoice I "
                                +"INNER JOIN Customer C ON C.CustomerId = I.CustomerId "
                                +"ORDER BY I.Total DESC";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine(reader.GetName(0) + " " + reader.GetName(1));
                            while (reader.Read())
                            {

                                CustomerSpender temp = new CustomerSpender();
                                temp.FirstName = reader.GetString(0);
                                temp.LastName = reader.GetString(1);
                                temp.Total = reader.GetValue(2).ToString();
                                customerSpender.Add(temp);
                                
                            }

                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            return customerSpender;
        }
        /// <summary>
        /// returns a customer and it's most popular gere by inserting a CustomorId
        /// </summary>
        /// <returns></returns>
        public List<CustomerGenre> GetCustomersGenre(int Id)//public List<Customers> GetCustomersInCountry()
        {
            List<CustomerGenre> customerGenre = new List<CustomerGenre>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConectionString()))
                {
                    connection.Open();
                    string sql = "SELECT TOP(1) WITH TIES  C.CustomerId,  C.FirstName, C.LastName, G.Name, COUNT( G.Name) AS NumberOfTracks FROM Customer C "
                                + "INNER JOIN Invoice AS I ON C.CustomerId = I.CustomerId "
                                + "INNER JOIN InvoiceLine AS L ON I.InvoiceId = L.InvoiceId "
                                + "INNER JOIN Track AS T ON T.TrackId = L.TrackId "
                                + "INNER JOIN Genre AS G ON T.GenreId = G.GenreId "
                                + $"WHERE C.CustomerId = {Id}  GROUP BY C.CustomerId, C.FirstName, C.LastName, C.CustomerId, G.Name  ORDER BY NumberOfTracks DESC";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine(reader.GetName(0) + " " + reader.GetName(1));
                            while (reader.Read())
                            {

                                CustomerGenre temp = new CustomerGenre();
                                temp.Id = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Genre = reader.GetString(3);
                                temp.PlayedTimes = reader.GetInt32(4);
                                customerGenre.Add(temp);

                            }

                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            return customerGenre;
        }
    }

}


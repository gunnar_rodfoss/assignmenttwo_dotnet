﻿using AssignmentTwo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTwo.Repositories
{
    public interface ICostumerRepositorie
    {
        public List<Customers> GetAllCustomers();
        public Customers GetCostumerById(int Id);

        public Customers GetCostumerByName(string name);

        public List<Customers> GetPageCustomers(int limit, int offset);

        public bool AddCustomer(Customers customer);

        public bool UpdateCustomer(Customers customer);

        public List<CustomerCountry> GetCustomersInCountry();

        public List<CustomerSpender> GetCustomersSpending();

        public List<CustomerGenre> GetCustomersGenre(int Id);


    }
}

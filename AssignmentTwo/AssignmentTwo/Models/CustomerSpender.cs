﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTwo.Models
{
    public class CustomerSpender
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Total { get; set; }
    }
}
